<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('home');
    }
    
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/units/{unit}/{lesson}', [
        'as'   => 'units',
        'uses' => 'LessonController@show'
    ]);

    Route::post('/units/{unit}/{lesson}', [
        'as'   => 'units.post',
        'uses' => 'LessonController@post'
    ]);

    Route::get('/complete', [
        'as'   => 'complete',
        'uses' => 'LessonController@complete'
    ]);

    Route::get('/questions/{questionId}', [
        'as'   => 'questions.ask',
        'uses' => 'QuestionsController@ask'
    ]);


    Route::post('/questions/{questionId}/answer', [
        'as'   => 'questions.answer',
        'uses' => 'QuestionsController@answer'
    ]);

    Route::group([
        'prefix' => 'admin',
        'middleware' => ['can:view,App\Admin']
    ], function () {
        Route::get('progress/{studentId}', [
            'uses' => 'ProgressController@show',
            'as' => 'progress.show'
        ]);
        Route::get('progress', [
            'uses' => 'ProgressController@index'
        ]);
        Route::get('/units', [
            'uses' => 'UnitController@index',
            'as' => 'units.index'
        ]);
        Route::get('/units/create', [
            'uses' => 'UnitController@getCreate',
            'as' => 'units.create'
        ]);
        Route::post('/units/create', [
            'as'   => 'units.post',
            'uses' => 'UnitController@postCreate'
        ]);
        Route::get('/questions', [
            'uses' => 'QuestionsController@index',
            'as' => 'questions.index'
        ]);
        Route::get('/questions/create', [
            'uses' => 'QuestionsController@getCreate',
            'as' => 'questions.create'
        ]);
        Route::post('/questions/create', [
            'as'   => 'questions.post',
            'uses' => 'QuestionsController@postCreate'
        ]);
    });
});
