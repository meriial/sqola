<?php

return [
    'functions1' => [
        'preamble' => 'Write a function named "bob" that returns the expected output. <br><br>Don\'t forget to echo the return results',
        'calc' => function ($input = null) {
            return 'Hello, World!';
        },
        'fileInput' => true,
    ],
    'functions2' => [
        'title' => 'Function Arguments',
        'preamble' => "You can pass variables into functions. These are called 'arguments' inside the function. They can have different names, but will have the same value.",
        'calc' => function ($input = null) {
            return 13;
        },
        'fileInput' => true,
    ],
    'functions3' => [
        'title' => 'Return Values',
        'preamble' => "An important aspect of functions, is the ability for it to RETURN some data back to where it was called.  This can be a simple value, a whole set of data, an object, or even another function!  You'll really need to master this in order to be able to follow the flow of information.",
        'calc' => function ($input = null) {
            return "Plasma Cartridge";
        },
        'fileInput' => true,
    ],
    'functions4' => [
        'title' => 'Keep it DRY, Baby',
        'preamble' => "DRY - Don't.  Repeat.  Yourself.  A key concept to the lazy programmer.  As we type zillions of lines of code, the beauty of a function is that it can be executed as many times as we need, when we need.  No point in typing the same thing twice, when you can avoid it!",
        'calc' => function ($input = null) {
            return 12500026.5;
        },
        'fileInput' => true,
    ],
    'functions5' => [
        'title' => 'Execution',
        'preamble' => "Just because a function CAN return a value, doesn't mean it needs to.  A procedure (or subroutine), is a function that doesn't return a value but simply executes some code, and then allows your script to carry on.",
        'calc' => function ($input = null) {
            return "Spock was traveling at the speed of light, and then he saw one of those badguys.";
        },
        'fileInput' => true,
    ],
    'functions6' => [
        'title' => 'Default Argument Values',
        'preamble' => "Sometimes you want to give a function the ability to use an argument, but you can't always provide one.  This is especially handy when a function is going to be used in a variety of ways.  By giving the argument a default value, the function can run regardless of what is provided as input.  Check it out.",
        'calc' => function ($input = null) {
            return "Your power level is OVER 9000!";
        },
        'fileInput' => true,
    ],
    'functions7' => [
        'preamble' => 'You can pass variables with default values into functions. When you call the function, you can pass in parameters or if you did not, the default value will be used.',
        'calc' => function($input = null) {
            return "The average height is: 5 feet.";
      },
      'fileInput' => true,
    ],
];
