<?php

return [
    'statements1' => [
        'title' => 'If Loops',
        'preamble' => 'Conditional statements are used when you want something to happen one time, and something else to happen the other time.  They are often used in conjunction with comparison operators to compare data in order to decide which path to take.',
        'calc' => function ($input = null) {
            return "The system says: Was it really ever a contest?";
        },
        'fileInput' => true,
    ],
    'statements2' => [
        'title' => 'If Else Loops',
        'preamble' => 'What if you need to check for multiple potential outcomes?  Luckily, If loops let you tack on additional comparisons by means of: else if.',
        'calc' => function ($input = null) {
            return "Obviously.";
        },
        'fileInput' => true,
    ]
];
