<?php

return [
    'objects1' => [
        'title' => 'Objects',
        'preamble' => "PHP is an Object Oriented programming language.  This means that you can create 'Objects' which contain their own variables and functions.  When these belong to an object, they're referred to as properties and methods, respectively.  This allows us to seperate logic and functionality into distinct groups, and gives your code a natural sense of organization.  An Object can be pretty much anything you wish, but to start let's think of a class as a 'Thing'.  A cat, a car, a granola bar... anything! <br><br>First things first - in order to use an object it must 'exist'.  We use Classes (Object Contructors) to create (or instantiate) an instance of the class by storing it in a variable.  Try that first!",
        'calc' => function ($input = null) {
            return 4;
        },
        'fileInput' => true,
    ],
    'objects2' => [
        'title' => 'Object Properties',
        'preamble' => "Now that we understand how Objects are created, we need to learn how to manipulate them.  You can access an object's properties or methods by using the arrow (->) syntax.  This will allow you to set them as you wish, or get previously set data.",
        'calc' => function ($input = null) {
            return "Blurple";
        },
        'fileInput' => true,
    ],
    'objects3' => [
        'title' => 'Object Methods',
        'preamble' => "Similar to properties, Object methods are called using the arrow (->) notation.  This allows you to call a function from within the object, one that is presumably related to the object in the first place.",
        'calc' => function ($input = null) {
            return "Beep Beep, I'm definitely not a Jeep!";
        },
        'fileInput' => true,
    ],
    'objects4' => [
        'title' => 'Object Scope',
        'preamble' => "A little trickier now.  Similar to the last exercise, we're calling an Object's properties and methods, but this time we're using the properties inside the Object itself, which requires a slightly different notation.<br><br>Since we're moving inside the object, we need to access it's own properties via the 'this' notation ( \$this->... ). 'This' is in reference to the object / class / or other entity that the code is currently running on.<br><br>Try it out!",
        'calc' => function ($input = null) {
            return "I am a Hot Pink car. I have 4 wheels.";
        },
        'fileInput' => true,
    ],
    'objects5' => [
        'title' => 'Object Constructors (and Loops)',
        'preamble' => "You can 'force' an object to have certain properties by requiring those arguments in the constructor. You can then access those properties in methods using \$this->propertyName.",
        'calc' => function ($input = null) {
            return "I am a Blue car, owned by Betty.\nI am a Red car, owned by Rob.\nI am a Green car, owned by Greta.\n";
        },
        'fileInput' => true,
    ],
    'objects6' => [
      'preamble' => "You can check if a property exists in a class.",
      'calc' => function ($input = null) {
          return "I have 4 sides.";
        },
      'fileInput' => true,
    ],
    'objects7' => [
      'preamble' => "You can also check if a method exists in a class.",
      'calc' => function ($input = null) {
          return "Hi, I'm a new student at SAIT.";
      },
      'fileInput' => true,
    ]
];
