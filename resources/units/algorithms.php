<?php

return [
    'algorithms1' => [
        'preamble' => 'Write a function such that it will take 3 numbers as parameters. <br><br> You will create a function that writes each number from the first number passed in (e.g 27) through 0. Except when the first number contains either of the other numbers, it will output FIZZ. When the first number is divisible by either of the other numbers, it will output BUZZ. If both are true it will output FIZZBUZZ.',
        'calc' => function ($input = null) {
            $i = [27, 3, 5];
            $output = [];
            for($j = $i[0]; $j > 0; $j--){
                $test = false;
                $iOutput = '';
                if(($j % 10) == $i[2] || ($j % 10) == $i[1] || floor($j/10) == $i[2] || floor($j/10) == $i[1]){
                    $test = true;
                    $iOutput = $iOutput . 'FIZZ';
                }
                if(!($j % $i[1]) || !($j % $i[2])){
                    $test = true;
                    $iOutput = $iOutput . 'BUZZ';
                }

                if(!$test){
                    $iOutput = $iOutput . $j;
                }

                array_push($output, $iOutput);
            }
            return join($output, ', ');
        },
        'fileInput' => true,
    ],
];
