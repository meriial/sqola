<?php

return [
  'strings1' => [
    'preamble' => 'Get the length of a string. Use the string\'s strlen() method.',
    'calc' => function ($input = null) {
      return 7;
    },
    'fileInput' => true,
  ],
  'strings2' => [
    'preamble' => 'Get the first position of a word from a given string. Use the string\'s strpos(param1, param2) method. Where param1 is the sring and param2 is the word.',
    'calc' => function ($input = null) {
      return 7;
    },
    'fileInput' => true,
  ],
  'strings3' => [
    'preamble' => 'Change a word from a string. <br/> Use the string\'s str_replace(param1, param2, param3) method. Where param1 is the word you want to change, param2 is the word you want it to change in and param3 is the string.',
    'calc' => function ($input = null) {
      return "Happy Holidays!";
    },
    'fileInput' => true,
  ]
]
