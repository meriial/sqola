<?php


$underTheBed = [
    "Old socks",
    "Half eaten pizza pop",
    "I don't even want to know what that is",
    "Probably a Monster",
    "Tuna can"
];

// Using a Foreach Loop, iterate over the items in $underTheBed and
// and print each item to the screen

foreach (tell the loop how to go through $underTheBed) {
    # code...
}
