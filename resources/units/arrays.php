<?php

return [
    'arrays1' => [
        'preamble' => "Arrays are data structures that contain groups of elements.  Each element of an array has an index (or location) with which it can be referred to. Let's try manipulating an array now - use the one provided to end up with what's expected.",
        'calc' => function ($input) {
            return 15;
        },
        'fileInput' => true
    ],
    'arrays2' => [
        'title' => 'Sequential Indexes',
        'preamble' => "Sequential arrays have indexes that start at 0 and increment by 1 for each additional element in the array.<br><br>That means that the 2nd element has an index of 1!! This is called zero-indexing.",
        'calc' => function ($input) {
            return 'anteaters';
        },
        'fileInput' => true
    ],
    'arrays3' => [
        'title' => 'Array Functions',
        'preamble' => "PHP has a number of functions to manipulate arrays. Use the join() function to echo out this array's elements, separated by commas. <br><br>You'll need to use a different technique to get the final '.'",
        'calc' => function ($input) {
            $array = [
                'hear no evil',
                'see no evil',
                'speak no evil'
            ];
            return join(', ', $array).'.';
        },
        'fileInput' => true
    ],
    'arrays4' => [
        'title' => 'Sorting Arrays',
        'preamble' => "Sorting arrays is easy. Use 'rsort()' to sort the previous array. Then use 'join()' again to make a pretty string.<br><br>Google 'php sort array functions' to see a fuller list.",
        'calc' => function ($input) {
            $array = [
                'hear no evil',
                'see no evil',
                'speak no evil'
            ];
            rsort($array);
            return join(', ', $array).'.';
        },
        'fileInput' => true
    ],
    'arrays5' => [
        'title' => 'Adding To Arrays',
        'preamble' => "There are few different ways to add elements to an array. Try using 'array_push()' to add a new element to the same array. Again, use 'join()' to make a pretty string.",
        'calc' => function ($input) {
            $array = [
                'hear no evil',
                'see no evil',
                'speak no evil'
            ];
            $array[] = 'smell no evil';
            $array[] = 'email no evil';

            return join(', ', $array).'.';
        },
        'fileInput' => true
    ],
    'arrays6' => [
        'preamble' => 'Write a foreach loop that takes the input and generates the expected output.',
        'calc' => function ($input = null) {
            foreach ($input['fruits'] as $value) {
                echo "$value\n";
            }
        },
        'input' => [
            'fruits' => [
                'apples',
                'bananas',
                'pears'
            ]
        ]
    ],
    'arrays7' => [
        'preamble' => 'Write a foreach loop that takes the input and generates the expected output.',
        'calc' => function ($input = null) {
            foreach ($input['fruits'] as $key => $value) {
                echo "$key -- $value\n";
            }
        },
        'input' => [
            'fruits' => [
                'apples' => 'red',
                'bananas' => 'yellow',
                'pears' => '???'
            ]
        ]
    ],
];
