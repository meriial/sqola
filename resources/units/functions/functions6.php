<?php


// We can give the $input argument a default by assigning it a value.
// Now, if we call this function and don't supply it with any arguments, it
// will just use the default.

function calculatePowerLevel(?????) {
    return "Your power level is ".$input;
}

// Try replacing the ????? above with an argument with a default value
// in order to end up with the Expected Results.

echo calculatePowerLevel();
