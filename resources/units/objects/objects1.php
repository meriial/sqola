<?php


class Car
{
    // Here we see some of the Car Object's Properties
    // Some of them are blank, others are filled because we've built
    // the Car Object assuming that all cars have 4 wheels

    public $colour = "";
    public $numberOfWheels = 4;
}

// Try 'newing up' a Car instance...

$instanceOfCar = ????

// And then echoing this particular Car's numberOfWheels!

// hint - you can access an Object's property or method using the
// arrow (->) syntax

echo $instanceOfCar????
