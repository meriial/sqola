<?php

class Car
{
    public $colour = "blah";
    public $numberOfWheels = 4;
}

// Now that we have this idea of a generic Car
$genericInstanceOfCar = new Car;

// Let's try creating a new Car, with specific properties.
$dreamCar = new Car;

// We want this Car to be different than the Generic one, so we set it's
// properties to reflect as such:

$theBestColourEver = "Blurple";

// Can you show that you've changed your dreamCar's colour property
// to $theBestColourEver?

$dreamCar??? = ???;

echo ???
