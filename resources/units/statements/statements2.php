<?php

// A quick survey of the SAIT campus yielded the following results:

$numberOfHockeyFans = 7;
$numberOfShuffleBoardFans = 254;
$numberOfHardcorePHPHackers = 9805;

// Write an if statement that doesn't pass right away,
// but executes on one if it's else if statments.

if (condition) {
    echo "Nope.";
} elseif (condition) {
    echo "No way, Jose.";
} elseif (condition) {
    echo "Obviously.";
}
