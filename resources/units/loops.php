<?php

return [
    'loops1' => [
        'preamble' => 'Write a for loop that generates the expected output.',
        'calc' => function ($input = null) {
            for ($i=0; $i < 8; $i++) {
                echo 'loop'.$i."\n";
            }
        },
        'input' => []
    ],
    'loops2' => [
        'preamble' => 'Write a nested for loop that generates the expected output.',
        'calc' => function ($input = null) {
            for ($i=0; $i < 2; $i++) {
                echo 'loop'.$i."\n";
                for ($j=0; $j < 3; $j++) {
                    echo "--$j\n";
                }
            }
        },
        'input' => []
    ],
    'loops3' => [
        'title' => 'Foreach Loops',
        'preamble' => "Foreach Loops are used to iterate over elements in an array, and perform an operation on each key/value pair individually.",
        'calc' => function ($input = null) {
            return "Old socksHalf eaten pizza popI don't even want to know what that isProbably a MonsterTuna can";
        },
        'fileInput' => true,
    ]
];
