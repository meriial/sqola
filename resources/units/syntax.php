<?php

return [
    'syntax1' => [
        'preamble' => "Let's start with something simple: Print 'Hello, World!'",
        'title' => 'Echo',
        'calc' => function ($input) {
            return "Hello, World!";
        },
        'input' => []
    ],
    'syntax2' => [
        'preamble' => "Let's try some operators. Use any combination of +, -, * and / with the provided variables to get the expected results.",
        'title' => 'Operators',
        'calc' => function ($input) {
            extract($input);
            return ($a + $b) * $c / ($d - $e);
        },
        'input' => [
            'a' => 10,
            'b' => 20,
            'c' => 5,
            'd' => 3,
            'e' => 2,
        ]
    ],
    'syntax3' => [
        'preamble' => "Strings are handy tidbits of characters in a row.  In fact, this very sentence is part of a string.  Sometimes, you need to manipulate these strings - try it now. Use the given data to create the expected one. Remember, spaces count as characters.",
        'calc' => function ($input) {
            return $input['w']." ".$input['o']." ".$input['r']." ".$input['d'].$input['s'];
        },
        'input' => [
            'r' => 'were so',
            'w' => 'Who knew',
            'd' => 'very interesting',
            'o' => 'that strings',
            's' => '?'
        ]
    ],
    'syntax4' => [
        'preamble' => "Operators are a way to compare things.  Comparing two or more variables will evaluate to either True (1), or False (0) depeding on what comparison is being made.  Quick, check that ROAM has more Water than redStuff? <br><br> Check out some more info here: http://php.net/manual/en/language.operators.comparison.php.",
        'calc' => function ($input) {
            return true;
        },
        'input' => [
            'redStuff' => 1000,
            'water' => 2000,
        ]
    ],
    'syntax5' => [
        'scenario' => 'The year is 2147.  The terraforming of Mars is well underway, and you are part of an away team that explores future expansion sites for the Earth Squad.  The ROAM (Radically Ordinary Astronaut Mobile) has begun to malfunction during a particularily dangerous mission, and an impending storm is forcing your crew to return to HQ.  Unfortunately, Commander Scrublord has gone completely blind AND lost the use of his fingertips from exposure to Noobium - a highly toxic Martian Anaconda venom.  You\'re the second in command, and Scrublord has activated you as Commading Officer.  As Scrublord settles into an adjacent seat you hear him mutter, "I hope you\'ve kept up with your training, Claytonius.  This whole rig runs on PHP - it\'s all up to you now." Time to take control of the ROAM and get your people back to safety.',
        'preamble' => "Start taking control of the ROAM by fixing all the syntax errors in this critical life support module and getting the output to match the expected results.",
        'calc' => function ($input) {
            echo "All clear!";
        },
        'fileInput' => true
    ]
];
