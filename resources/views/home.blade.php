@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default margin-top-20">
                <div class="panel-heading">
                    <h1>Available Units</h1>
                </div>

                <div class="panel-body padding-40">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($units)
                        <ul class="list-unstyled">
                            @foreach ($units as $unit)
                                <?php $slug = str_slug($unit->name); ?>
                                <li class="margin-bottom-20">
                                    <a href="{{ route('units', ["{$slug}", 1]) }}" class="btn btn-block btn-lg btn-primary">{{ strtoupper($unit->name) }}</a>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <h1>There are currently no available Units.</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
