@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{ route('units.create') }}" class="btn btn-default pull-right">Create Unit</a>
        <h1>Units</h1>
        <table>
            @foreach ($units as $unit)
                <tr>
                    <td>{{ $unit->name }}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
