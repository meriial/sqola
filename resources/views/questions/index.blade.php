@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{ route('questions.create') }}" class="btn btn-default pull-right">Create Question</a>
        <h1>Questions</h1>
        <table>
            @foreach ($questions as $question)
                <tr>
                    <td>{{ $question->title }}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
