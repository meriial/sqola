@extends('layouts.app')

@section('content')
    <form class="" action="{{ route('questions.answer', $question['id']) }}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="answer">{{ $question['title'] }}</label>
            <input type="text" name="answer" class="form-control" />
        </div>
        <input type="submit" name="submit" value="Submit" class="btn btn-default"/>
    </form>
@endsection
