@if( $message = Session::get('message') )
    <section class="message-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="list-unstyled">
                        <li class="message-head"><h2><i class="fa fa-info-circle"></i> Notice:</h2></li>
                        <li class="message-content">{!! $message !!}</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endif
