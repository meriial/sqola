@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="answers table">
            @foreach ($report->answeredQuestions as $answer)
                <tr>
                    <td>{{ $answer->id }}</td>
                    <td class="question">{{ $answer->question }}</td>
                    <td class="answer">{{ $answer->answer }}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
