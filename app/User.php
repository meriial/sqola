<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\ProgressPoint;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function progressPoints()
    {
        return $this->hasMany(ProgressPoint::class);
    }

    public function answer($unit, $question)
    {
        $progressPoint = ProgressPoint::where('user_id', $this->user->id)
            ->where('unit_id', $unit->id)
            ->where('question_id', $question->id)
            ->first();

        if (!$progressPoint) {
            $progressPoint = new ProgressPoint();
            $progressPoint->user_id = $this->user->id;
            $progressPoint->unit_id = $unit->id;
            $progressPoint->question_id = $question->id;
        }

        $progressPoint->attempts = $progressPoint->attempts++;
        $progressPoint->save();
    }

    public function markCompleted($unit, $question)
    {
        $progressPoint = ProgressPoint::where('user_id', $this->user->id)
            ->where('unit_id', $unit->id)
            ->where('question_id', $question->id)
            ->first();

        $progressPoint->completed = true;
        $progressPoint->save();
    }

    public function mostRecentProgressPoint()
    {
        $latest = ProgressPoint::where('user_id', $this->user->id)->last();

        return $latest;
    }
}
