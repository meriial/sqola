<?php

namespace App\Models;

class Unit
{
    public static function from($unitSlug)
    {
        $unitConfig = include base_path('resources/units/'.$unitSlug.'.php');

        return new static($unitSlug, $unitConfig);
    }

    public function __construct($slug, $config)
    {
        $this->slug = $slug;
        $this->config = $config;
    }

    public function next($lessonIndex)
    {
        if ($lessonIndex < count($this->config)) {
            return [$this->slug, ++$lessonIndex];
        } else {
            return [$this->slug, 1];
        }
    }
}
