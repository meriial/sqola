<?php

namespace App\Models;
use App\Models\Unit;

class Lesson
{
    public static function from($unit, $index)
    {
        $unit = include base_path('resources/units/'.$unit.'.php');

        $lessonConfig = $unit[$index-1];

        return new static($lessonConfig);
    }

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function expected()
    {
        $input = $this->config['input'];
        $calc = $this->config['calc'];

        return $calc($input);
    }

    public function input()
    {
        return $this->config['input'];
    }

    public function preamble()
    {
        return $this->config['preamble'] ?? 'Try another one!';
    }
}
