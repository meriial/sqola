<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DbRepo;
use Sqola\Commands\GetUnits;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repo = new DbRepo();
        $getUnits = new GetUnits($this->repo);
        $units = $getUnits->execute();

        return view('home', [
            'units' => $units
        ]);
    }
}
