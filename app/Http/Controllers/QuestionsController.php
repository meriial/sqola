<?php

namespace App\Http\Controllers;

use App\Factories\DatabaseProblemFactory;
use App\Repositories\DbRepo;
use Illuminate\Http\Request;
use Sqola\Commands\AnswerProblem;
use Sqola\Commands\CreateProblem;
use Sqola\Commands\GetQuestion;
use Sqola\Commands\GetUnits;

class QuestionsController extends Controller
{
    public function index()
    {
        $questions = \DB::table('problems')->get();

        return view('questions.index', compact('questions'));
    }

    public function answer($questionId, Request $request)
    {
        \Log::error('in QuestionsController', $request->all());
        $this->repo = new DbRepo();
        $answer     = new AnswerProblem($this->repo);
        $answer->execute([
            'student_id'   => $request->user()->id,
            'answer'       => $request->answer,
            'problem_id'   => $questionId,
            'lesson_index' => $questionId,
            'unit_slug'    => 'syntax',
        ]);
    }

    public function ask($questionId)
    {
        $this->repo = new DbRepo();

        $getQuestion = new GetQuestion($this->repo);
        $question    = $getQuestion->execute([
            'question_id' => $questionId
        ]);

        return view('questions.ask', [
            'question' => $question
        ]);
    }

    public function getCreate()
    {
        $this->repo = new DbRepo();
        $getUnits   = new GetUnits($this->repo);
        $units      = $getUnits->execute();

        return view('admin.questions', [
            'units' => $units
        ]);
    }

    public function postCreate(Request $request)
    {
        $this->repo     = new DbRepo();
        $problemFactory = new DatabaseProblemFactory();

        $createProblem = new CreateProblem($this->repo, $problemFactory);
        $createProblem->execute([
            'title'     => $request->title,
            'config_id' => $request->config_id,
            'unit_id'   => $request->unit_id,
        ]);

        return redirect()->route('questions.index');
    }
}
