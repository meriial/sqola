<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\DatabaseUnitFactory;
use App\Repositories\DbRepo;
use Sqola\Commands\CreateUnit;

class UnitController extends Controller
{
    public function index()
    {
        $units = \DB::table('units')->get();

        return view('units.index', compact('units'));
    }

    public function getCreate()
    {
        return view('admin.units');
    }

    public function postCreate(Request $request)
    {
        $this->repo = new DbRepo();
        $unitFactory = new DatabaseUnitFactory();

        $createUnit = new CreateUnit($this->repo, $unitFactory);
        $createUnit->execute([
            'name' => $request->name,
            'slug' => str_slug($request->name)
        ]);

        return redirect()->route('units.index');
    }
}
