<?php

namespace App\Tests\Helpers;

trait Errors
{
    public function assertNoPhpErrors($displayShortError = true)
    {
        $traceLimit = 5;
        $page       = $this->getSession()->getPage();
        $text       = $page->getText();

        if ($this->containsError($text)) {
            if ($displayShortError) {
                $message = $page->find('css', '.exc-message > span')->getText();
                echo "\n".$this->getSession()->getCurrentUrl()."\n";
                echo "\n".$message."\n";
                $trace = $page->findAll('css', '.frames-container .frame');
                for ($i=0; $i < $traceLimit; $i++) {
                    if (!isset($trace[$i]) || empty($trace[$i]->getText())) {
                        break;
                    }
                    echo "\n-- ".$trace[$i]->getText();
                }
            } else {
                $this->printLastResponse();
            }
            throw new \Exception("There was a PHP error!");
        }
    }

    private function containsError($text)
    {
        return preg_match('/A PHP Error was encountered/', $text)
            || preg_match('/Parse error/', $text)
            || preg_match('/Exception/', $text)
            || preg_match('/Fatal error/', $text)
            // || preg_match('/Notice:/', $text )
            || preg_match('/Whoops, looks like something went wrong./', $text)
            || preg_match('/Exception in/', $text);
    }

    public function assertNotPageAddress($url)
    {
        try {
            $this->assertPageAddress($url);
        } catch (\Exception $e) {
            return;
        }

        $this->printLastResponse();
        throw new \Exception("Page was '$url' but should not have been.");
    }

    public function assertPageAddress($url)
    {
        $this->assertNoPhpErrors();
        parent::assertPageAddress($url);
    }

    public function visit($url, $expectErrors = false)
    {
        parent::visit($url);

        if (!$expectErrors) {
            $this->assertNoPhpErrors();
            // $this->assertResponseStatus(200);
        }
    }

    public function pressButton($name)
    {
        parent::pressButton($name);

        $this->assertNoPhpErrors();
    }
}
