<?php

namespace App\Tests\Helpers;

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ApiClient extends TestCase
{
    // use WithoutMiddleware;

    public function __construct()
    {
        $this->setUp();
    }

    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    public function setToken(string $token)
    {
        $this->token = $token;
    }

    public function app()
    {
        return $this->app;
    }

    public function home()
    {
        return $this
            ->get('/');
    }

    public function answerProblem($input)
    {
        extract($input);

        $user = User::find($student_id);

        $this
            ->withSession(['_token' => 'haha'])
            ->actingAs($user);

        $url = "questions/$problem_id/answer";

        $response = $this->json('POST', $url, [
            'answer' => $answer,
            '_token' => 'haha'
        ]);

        return $response;
    }

    public function answerQuestion($input)
    {
        extract($input);

        $user = User::find($student_id);

        $this
            ->withSession(['_token' => 'haha'])
            ->actingAs($user);

        $url = "units/$unit_id/$question_id";

        $response = $this->json('POST', $url, [
            'code' => $code,
            '_token' => 'haha'
        ]);

        $this->validateJsonResponse($response);

        return $response;
    }

    public function showQuestion($input)
    {
        extract($input);

        $user = User::find($studentId);

        $this->actingAs($user);

        $url = "units/$unitId/$questionId";

        $response = $this->get($url);

        $this->validateHtmlResponse($response);

        return $response;
    }

    public function validateJsonResponse($response)
    {
        $data = $response->getData() ?? false;

        if (!$data) {
            throw new \Exception('JSON response does not have data.');
        }

        if ($data && isset($data->exception)) {
            var_dump([
                'message'   => $data->message,
                'exception' => $data->exception,
                'file'      => $data->file,
                'line'      => $data->line
            ]);
            throw new \Exception('JSON request returned 500 error: '.$data->exception);
        }
    }

    public function validateHtmlResponse($response)
    {
        if (isset($response->exception)) {
            var_dump([
                'message'   => $response->exception->getMessage(),
                'exception' => get_class($response->exception),
                'file'      => $response->exception->getFile(),
                'line'      => $response->exception->getLine()
            ]);
            $trace = $response->exception->getTrace();
            for ($i=0; $i < 6; $i++) {
                $t = $trace[$i];
                echo '-'.($t['file'] ?? '??').':'.($t['line'] ?? '')."\n";
            }
            throw new \Exception("Bad Response: ".get_class($response->exception));
        }
    }

    public function getReport($payload)
    {
        $studentId = $payload['studentId'];

        $response = $this->json("GET", "admin/progress/$studentId");

        return $response;
    }

    public function getAllReports()
    {
        $response = $this->get("admin/progress");

        return $response;
    }

    public function createUnit($input)
    {
        extract($input);

        $this
            ->withSession(['_token' => 'haha'])
            ->actingAs($user);

        $url = "units/create";

        $response = $this->json('POST', $url, [
            'name'   => $name,
            '_token' => 'haha'
        ]);

        return $response;
    }
}
