<?php

namespace App\Tests\Factories;

use Sqola\Entities\Student;
use Sqola\Contracts\UserFactory;

class InMemoryUserFactory implements UserFactory
{
    public function makeStudent($payload): Student
    {
        $id = rand(1, 10000);

        return new Student($id, $payload['name'], $payload['email']);
    }
}
