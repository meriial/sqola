<?php

namespace App\Tests\Pages;

class Register extends Page
{
    public function registerUser($name, $email, $password)
    {
        $this->visit('register');
        $this->fillField('name', $name);
        $this->fillField('email', $email);
        $this->fillField('password', $password);
        $this->fillField('password_confirmation', $password);
        $this->pressButton('Register');
    }
}
