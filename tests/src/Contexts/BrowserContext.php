<?php

namespace App\Tests\Contexts;

use App\Factories\DatabaseProblemFactory;
use App\Repositories\DbRepo;
use App\Tests\Factory;
use App\Tests\Helpers\PHPUnit;
use App\Tests\Pages\Home;
use App\Tests\Pages\Register;
use App\Tests\Pages\Unit;
use App\User;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\TableNode;

use Behat\Mink\Session;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Testwork\Hook\Scope\AfterSuiteScope;
use Behat\Testwork\Hook\Scope\BeforeSuiteScope;
use Faker\Factory as Faker;
use Sqola\Commands\CreateProblem;

class BrowserContext extends MinkContext implements Context, SnippetAcceptingContext
{
    // use \App\Tests\Contexts\Traits\Assertions;
    use \App\Tests\Helpers\Errors;

    // use \Laracasts\Behat\Context\DatabaseTransactions;
    // use \Laracasts\Behat\Context\Migrator;
    use PHPUnit;

    public $wait = 0;
    public $hookHandler;

    public function __construct($wd_host, $capabilities)
    {
        $this->wd_host      = $wd_host;
        $this->capabilities = $capabilities;
        $this->faker        = (new Faker())->create();
    }

    /** @BeforeSuite */
    public static function setup(BeforeSuiteScope $scope)
    {
        \Log::error('copying .env');
        copy('.env.behat', '.env');
    }

    /** @AfterSuite */
    public static function teardown(AfterSuiteScope $scope)
    {
        \Log::error('copying .env back');
        copy('.env.local', '.env');
    }

    /**
     * @BeforeScenario @browser
     */
    public function beforeScenario(BeforeScenarioScope $scope)
    {
        $this->setSession('default');
    }

    public function setSession($name)
    {
        $mink = $this->getMink();

        if (!$mink->hasSession($name)) {
            echo "------ Registering session for $name\n";

            $driver = new \Behat\Mink\Driver\Selenium2Driver(
                'chrome',
                $this->capabilities,
                $this->wd_host
            );

            $session = new \Behat\Mink\Session($driver);
            $mink->registerSession($name, $session);
        }

        $mink->setDefaultSessionName($name);

        return $mink->getSession();
    }

    /**
     * @Then as member :arg1
     */
    public function asMember($name)
    {
        $this->setSession($name);
    }

    /**
     * @When I log in as :arg1
     */
    public function iLogInAs($memberName)
    {
        $session = $this->setSession($memberName);

        $session->reset();
        $this->visit('login');
        // $this->printLastResponse();
        $this->fillField('email', $memberName.'@sqola.com');
        $this->fillField('password', 12341234);
        $this->pressButton('Login');
        $this->waitForElementToDisappear('.loading');
        // $this->tapTaskboard();
        // $this->clickElement('.module-tile-realtime');
        // $this->waitForElementToDisappear('.loading');
        // $this->printLastResponse();
    }

    public function iLogInAsUser(User $user)
    {
        $session = $this->setSession($user->name);
        $session->reset();

        $this->visit('login');
        $this->assertPageAddress('login');
        // $this->printLastResponse();
        \Log::error('logging in as '.$user->email);
        $this->fillField('email', $user->email);
        $this->fillField('password', '12341234');
        $this->pressButton('Login');
        $this->waitForElementToDisappear('.loading');
        $this->assertPageAddress('home');
    }

    public function clickElement($selector, $shouldScroll = false)
    {
        echo "------ clicking $selector\n";
        $this->iWaitSeconds($this->wait);

        $element = $this->getSession()->getPage()->find('css', $selector);

        if ($shouldScroll) {
            $scroll = "$('$selector')[0].scrollIntoView(true)";
            echo "--------- Scrolling into view with $scroll\n";
            $this->waitForExpression($scroll, 5);
        }

        $element->click();
    }

    public function waitForElementToDisappear($selector, $timeout = 30)
    {
        $page = $this->getSession()->getPage();

        // echo $page->getHtml()."\n==================\n";

        $expression = "$('$selector').is(':visible')";

        $result = $this->waitForExpression($expression, $timeout);

        if (!$result) {
            throw new \Exception("Element $selector did not disappear after $timeout seconds.");
        }
    }

    public function waitForElementToAppear($selector, $timeout = 30)
    {
        $page = $this->getSession()->getPage();

        // echo $page->getHtml()."\n==================\n";

        $expression = "!$('$selector').is(':visible')";

        $result = $this->waitForExpression($expression, $timeout);

        if (!$result) {
            throw new \Exception("Element $selector did not disappear after $timeout seconds.");
        }
    }

    public function waitForExpression($expression, $timeout)
    {
        $page = $this->getSession()->getPage();

        $result = $page->waitFor($timeout, function () use ($expression) {
            echo "------ waiting for expression to be FALSE... $expression\n";
            $elementIsVisible = $this
                ->getSession()
                ->evaluateScript($expression);

            echo !$elementIsVisible ? "\tok, stop waiting \n" : "\twait some more \n";
            return !$elementIsVisible;
        });

        return $result;
    }

    /**
     * @Then I wait :arg1 seconds
     */
    public function iWaitSeconds($seconds)
    {
        $this->getSession()->wait($seconds * 1000);
    }

    public function waitForTextToAppear($text, $timeout = 30)
    {
        $page = $this->getSession()->getPage();

        $result = $page->waitFor($timeout, function () use ($text) {
            // echo "------ about to run... $expression\n";
            echo "----- checking for $text, has it appeared? \n";
            $el = $this->elementWithText($text, false);
            echo !$el ? "\tyes \n" : "\tno \n";

            return !$el;
        });

        return $result;
    }

    public function waitForTextToDisappear($text, $timeout = 30)
    {
        $page = $this->getSession()->getPage();

        $result = $page->waitFor($timeout, function () use ($text) {
            // echo "------ about to run... $expression\n";
            echo "----- checking for $text, has it disappeared? \n";
            $el = $this->elementWithText($text, false);
            echo $el ? "\tyes \n" : "\tno \n";

            return $el;
        });

        return $result;
    }

    public function tap($text, $shouldScroll = false)
    {
        echo "------- tapping '$text'\n";
        $this->iWaitSeconds($this->wait);
        return $this->clickElementWithText($text, $shouldScroll);
    }

    public function elementWithText($text, $throwExceptions = true)
    {
        $page = $this->getSession()->getPage();

        $xpath = "//*[text()[contains(.,'$text')]]";

        $el = $page->find('xpath', $xpath);

        if (!$el && $throwExceptions) {
            throw new \Exception("Element with text '$text' was not found.");
        }

        return $el;
    }

    public function scrollElementIntoView($el)
    {
        $xpath = $el->getXpath();
        // echo "------- xpath: $xpath\n";
        $scroll = 'document.evaluate("'.$xpath.'", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true)';

        echo "--------- Scrolling into view with $scroll\n";

        $this->waitForExpression($scroll, 15);
    }

    public function clickElementWithText($text, $shouldScroll = false)
    {
        $el = $this->elementWithText($text);

        if ($shouldScroll) {
            $this->scrollElementIntoView($el);
        }

        $el->click();
    }

    /** @BeforeScenario */
    public function before(BeforeScenarioScope $scope)
    {
        \Artisan::call('migrate:fresh');
    }


    public function alert($message)
    {
        $expression = "alert('$message');";
        $this
            ->getSession()
            ->evaluateScript($expression);
    }

    /**
     * @When I create an account for student :arg1
     */
    public function iCreateAnAccountForStudent($name)
    {
        $register = new Register($this);
        $register->registerUser(
            $name,
            'ryanmlerner+'.$name.'@gmail.com',
            12341234
        );
    }

    /**
     * @Then I can select a unit to start
     */
    public function iCanSelectAUnitToStart()
    {
        $home = new Home($this);
        $home->assertUnits([
            'Syntax I'
        ]);
    }

    /**
     * @When I select unit :arg1
     */
    public function iSelectUnit($unit)
    {
        $home = new Home($this);
        $home->selectUnit($unit);
    }

    /**
     * @Then I am presented with a problem
     */
    public function iAmPresentedWithAProblem()
    {
        $this->assertPageContainsText('Solve the problem');
    }

    /**
     * @When I solve the problem
     */
    public function iSolveTheProblem()
    {
        $unit = new Unit($this);
        $unit->assertUnit('Syntax I');
        $unit->submitCode('echo exec("whoami");');
        $unit->verifyOutput('Hello!');
        sleep(20);
    }

    /**
     * @Then I am presented with the next problem
     */
    public function iAmPresentedWithTheNextProblem()
    {
        $unit = new Unit($this);
        $unit->assertUnit('Syntax II');
    }

    /**
     * @Given there is a student :arg1
     */
    public function thereIsAStudent($name)
    {
        $this->students[] = factory(User::class)->create([
            'name'     => $name,
            'email'    => $name.'+'.$this->faker->unique()->randomNumber.'@sqola.com',
        ]);
    }

    /**
     * @Given student :arg1 has completed :arg2 questions
     */
    public function studentHasCompletedQuestions($name, $numberOfQuestions)
    {
        $this->iLogInAs($name);
        $this->visit('/units/tests/1');
        for ($i=0; $i < $numberOfQuestions; $i++) {
            $this->pressButton('submit');
        }
    }

    /**
     * @When an admin views the progress area
     */
    public function anAdminViewsTheProgressArea()
    {
        $this->admin = factory(User::class)->create([
            'is_admin' => true
        ]);

        $this->iLogInAsUser($this->admin);
    }

    /**
     * @Then student :arg1 shows :arg2 questions completed
     */
    public function studentShowsQuestionsCompleted($index, $num)
    {
        $student = $this->students[$index - 1];
        $this->visit('admin/progress');
        $table = $this->getSession()->getPage()->find('css', '.students');
        $this->assertNotNull($table, 'Did not see a table of students');
        $trs = $table->findAll('css', 'tr');

        foreach ($trs as $tr) {
            if ($tr->getAttribute('class') == 's'.$student->id) {
                $this->assertEquals($num, $tr->find('css', '.count')->getText() ?? false);
                return;
            }
        }

        throw new \Exception("Student {$student->name} not found.");
    }

    /**
     * @Given student :arg1 has completed the following questions
     */
    public function studentHasCompletedTheFollowingQuestions($index, TableNode $table)
    {
        $problemFactory = new DatabaseProblemFactory;
        $this->repo     = new DbRepo();

        $student = $this->students[$index - 1];
        $this->iLogInAsUser($student);

        foreach ($table->getHash() as $row) {
            $createProblem = new CreateProblem($this->repo, $problemFactory);
            $problemId     = $createProblem->execute([
                'unit_id'   => $row['Unit ID'],
                'title'     => $row['Question'],
                'config_id' => 'syntax.syntax1'
            ]);

            $this->problemIds[] = $problemId;

            $this->visit('questions/'.$problemId);
            $this->fillField('answer', $row['Answer']);
            $this->pressButton('Submit');
        }
    }

    /**
     * @Then the report for student :arg1 shows:
     */
    public function theReportForStudentShows($index, TableNode $hash)
    {
        $student = $this->students[$index - 1];

        $this->iLogInAsUser($this->admin);
        $this->visit('admin/progress/'.$student->id);

        $table = $this->getSession()->getPage()->find('css', '.answers');
        $this->assertNotNull($table);
        $trs = $table->findAll('css', 'tr');

        foreach ($hash->getHash() as $row) {
            $this->assertRow($row, $trs);
        }
    }

    private function assertRow($row, $trs)
    {
        $foundMatch = false;

        foreach ($trs as $tr) {
            $actualQuestion   = $tr->find('css', '.question')->getText();
            $actualAnswer     = $tr->find('css', '.answer')->getText();
            $expectedQuestion = $row['Question'];
            $expectedAnswer   = $row['Answer'];

            if ($expectedAnswer == $actualAnswer && $expectedQuestion == $actualQuestion) {
                $foundMatch = true;
            }
        }

        if (!$foundMatch) {
            throw new \Exception("Did not find entry with answer: '$expectedAnswer' and question: '$expectedQuestion'.");
        }
    }

    /**
     * @When I change question :arg1 to :arg2
     */
    public function iChangeQuestionTo($title, $newTitle)
    {
        foreach ($this->problemIds as $id) {
            $existingProblem = $this->repo->problemWithId($id, $title);
            if ($existingProblem->title == $title) {
                $existingProblem->title = $newTitle;
                $this->repo->updateProblem($existingProblem);
            }
        }
    }

    /**
     * @Given there are the following units
     */
    public function thereAreTheFollowingUnits(TableNode $table)
    {
        $this->admin = factory(User::class)->create([
            'is_admin' => true
        ]);

        $this->iLogInAsUser($this->admin);

        foreach ($table->getHash() as $row) {
            $this->visit('/admin/units/create');
            $this->fillField('name', $row['Name']);
            $this->pressButton('Save');
        }
    }

    /**
     * @Given there is a unit :arg1 with questions
     */
    public function thereIsAUnitWithQuestions($unitName)
    {
        $this->admin = factory(User::class)->create([
            'is_admin' => true
        ]);
        $this->iLogInAsUser($this->admin);

        $this->visit('/admin/units/create');
        $this->fillField('name', $unitName);
        $this->pressButton('Save');

        $this->questions = $questions = [
            'syntax.syntax1'
        ];

        foreach ($questions as $question) {
            $this->visit('/admin/questions/create');
            $this->fillField('title', $this->faker->sentence);
            $this->fillField('config_id', $question);
            $this->selectOption('unit_id', $unitName);
            $this->pressButton('Save');
        }
    }

    /**
     * @When I log in as a student
     */
    public function iLogInAsAStudent()
    {
        $this->thereIsAStudent('student');

        $student = $this->students[0];

        $this->iLogInAsUser($student);
    }

    /**
     * @When I answer all the questions for unit :arg1
     */
    public function iAnswerAllTheQuestionsForUnit($unitName)
    {
        $this->visit('/home');
        $this->clickLink($unitName);

        foreach ($this->questions as $question) {
            $session = $this->getSession();
            $editor  = $session->getPage()->find('css', '.editor-class');

            $session->executeScript('
                ed.focus()
                ed.navigateFileEnd()
                ed.insert("echo $i;")
            ');

            $this->pressButton('Submit');
            $this->assertQuestionPassed();
            $this->clickLink('NEXT');
        }

        $this->assertPageContainsText('All Done.');
    }

    public function assertQuestionFailed()
    {
        $this->waitForElementToAppear('.modal-wrapper');
        $this->waitForTextToAppear('Try again.');
    }

    public function assertQuestionPassed()
    {
        $this->waitForElementToAppear('.modal-wrapper');
        $this->waitForTextToAppear('Success!');
        sleep(10);
    }

    /**
     * @Then the admin can see my progress
     */
    public function theAdminCanSeeMyProgress()
    {
        $this->iLogInAsUser($this->admin);
    }

    /**
     * @When I am an admin
     */
    public function iAmAnAdmin()
    {
        $this->admin = factory(User::class)->create([
            'is_admin' => true
        ]);

        $this->iLogInAsUser($this->admin);
    }

    /**
     * @Then I can view student progress
     */
    public function iCanViewStudentProgress()
    {
        $this->visit('admin/progress');
        $this->assertPageNotContainsText('This action is unauthorized.');
    }

    /**
     * @Then I can manage units and questions
     */
    public function iCanManageUnitsAndQuestions()
    {
        $this->visit('/admin/units');
        $this->assertPageNotContainsText('This action is unauthorized.');

        $this->visit('/admin/questions');
        $this->assertPageNotContainsText('This action is unauthorized.');
    }

    /**
     * @When I am not an admin
     */
    public function iAmNotAnAdmin()
    {
        $this->student = factory(User::class)->create();

        $this->iLogInAsUser($this->student);
    }

    /**
     * @Then I cannnot view student progress
     */
    public function iCannnotViewStudentProgress()
    {
        $this->visit('admin/progress');
        $this->assertPageContainsText('This action is unauthorized.');
    }

    /**
     * @Then I cannnot manage units and questions
     */
    public function iCannnotManageUnitsAndQuestions()
    {
        $this->visit('/admin/units');
        $this->assertPageContainsText('This action is unauthorized.');

        $this->visit('/admin/questions');
        $this->assertPageContainsText('This action is unauthorized.');
    }

    /**
     * @Given there is a unit :arg1 with :arg2 questions
     */
    public function thereIsAUnitWithQuestions2($unitName, $numberOfQuestions)
    {
        $this->admin = factory(User::class)->create([
            'is_admin' => true
        ]);
        $this->iLogInAsUser($this->admin);

        $this->visit('/admin/units/create');
        $this->fillField('name', $unitName);
        $this->pressButton('Save');

        // $this->questions = $questions = [
        //     'syntax.syntax1'
        // ];

        for ($i=1; $i <= $numberOfQuestions; $i++) {
            $this->visit('/admin/questions/create');
            $this->fillField('title', $this->faker->sentence);
            $this->fillField('config_id', 'syntax.syntax'.$i);
            $this->selectOption('unit_id', $unitName);
            $this->pressButton('Save');
        }
    }

    /**
     * @When I start a lesson
     */
    public function iStartALesson()
    {
        $this->visit('/home');
        $this->clickLink('syntax');
    }

    /**
     * @Then I can see question :arg2 from unit :arg1
     */
    public function iCanSeeQuestionFromUnit($questionNumber, $unitName)
    {
        $this->assertPageAddress("/units/{$unitName}/{$questionNumber}");
        sleep(3);
    }

    /**
     * @Then I answer question :arg2 for unit :arg1
     */
    public function iAnswerQuestionForUnit($questionNumber, $unitName)
    {
        $session = $this->getSession();
        $editor  = $session->getPage()->find('css', '.editor-class');

        $session->executeScript('
            ed.focus()
            ed.navigateFileEnd()
            ed.insert("echo $i;")
        ');

        sleep(3);

        $this->pressButton('Submit');
        $this->assertQuestionPassed();
        $this->clickLink('NEXT');

        sleep(3);
    }
}
