<?php

namespace App\Tests\Contexts\Traits;

trait Assertions
{
    public function pressButton($text, $checkForErrors = true)
    {
        parent::pressButton($text);

        if ($checkForErrors) {
            \Log::error('pressButton, checking for errors');
            $this->assertNoPhpErrors();
        }
    }

    public function assertPageContainsText($text)
    {
        $this->assertNoPhpErrors();
        parent::assertPageContainsText($text);
    }

    public function assertNoPhpErrors($displayShortError = true)
    {
        $traceLimit = 5;
        $page       = $this->getSession()->getPage();
        $text       = $page->getText();

        if ($this->containsPhpError($text)) {
            \Log::error('page contains errors!');
            if ($displayShortError) {
                $title   = $page->find('css', '.exc-title')->getText();
                $message   = $page->find('css', '.exc-message')->getText();
                echo "\n".$this->getSession()->getCurrentUrl()."\n";
                echo "\n".$title."\n".$message."\n";
                $trace = $page->findAll('css', '.frames-container .frame');
                for ($i=0; $i < $traceLimit; $i++) {
                    echo "\n-- ".$trace[$i]->getText();
                }
            } else {
                $this->printLastResponse();
            }
            throw new \Exception("There was a PHP error!");
        }
    }

    private function containsPhpError($text)
    {
        \Log::error('checking text: '.$text);
        return preg_match('/A PHP Error was encountered/', $text)
            || preg_match('/Parse error/', $text)
            || preg_match('/All frames/', $text)
            || preg_match('/Fatal error/', $text)
            // || preg_match('/Notice:/', $text )
            || preg_match('/Whoops, looks like something went wrong./', $text)
            || preg_match('/Exception in/', $text);
    }
}
