<?php

namespace App\Tests\Contexts;

use Behat\Behat\Tester\Exception\PendingException;
use App\Factories\DatabaseProblemFactory;
use App\Factories\DatabaseUnitFactory;
use App\Factories\DatabaseUserFactory;

use App\Repositories\DbRepo;
use App\Tests\Helpers\ApiClient;
use App\Tests\Helpers\PHPUnit;
use App\User;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Testwork\Hook\Scope\AfterSuiteScope;
use Behat\Testwork\Hook\Scope\BeforeSuiteScope;
use Faker\Factory;
use Illuminate\Support\Facades\Artisan;
use Sqola\Commands\CreateProblem;
use Sqola\Commands\CreateUnit;
use Sqola\Commands\GetUnitQuestions;
use Sqola\Commands\MakeStudent;

/**
 * Defines application features from the specific context.
 */
class ApiContext extends MinkContext implements Context
{
    use PHPUnit;

    public $studentIds = [];
    public $problemIds = [];
    public $unitIds    = [];

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct($repoContext = 'memory')
    {
        $this->faker  = Factory::create();
        $this->repo   = new DbRepo();
        $this->client = new ApiClient;
    }

    /** @BeforeSuite */
    public static function setup(BeforeSuiteScope $scope)
    {
        \Log::error('API Context — copying .env');
        // copy('.env', '.env.temp');
        // copy('.env.behat', '.env');
        \Log::error('API Context — resetting database');
        Artisan::call('migrate:fresh');
    }

    /** @AfterSuite */
    public static function teardown(AfterSuiteScope $scope)
    {
        \Log::error('API Context — copying .env back');
        // copy('.env.temp', '.env');
    }

    /**
     * @Given there is a student :arg1
     */
    public function thereIsAStudent($studentName)
    {
        $userFactory = new DatabaseUserFactory;
        $makeStudent = new MakeStudent($this->repo, $userFactory);

        $id = $makeStudent->execute([
            'name'     => $studentName,
            'email'    => $this->faker->email,
            'password' => '12341234'
        ]);

        $this->studentIds[] = $id;

        return $id;
    }

    /**
     * @When an admin views the progress area
     */
    public function anAdminViewsTheProgressArea()
    {
        $this->admin = factory(User::class)->create([
            'is_admin' => true
        ]);

        $this->client->actingAs($this->admin);
    }

    /**
     * @Then student :arg1 shows :arg2 questions completed
     */
    public function studentShowsQuestionsCompleted($index, int $num)
    {
        $studentId = $this->studentIds[$index - 1];
        $response  = $this->client->getAllReports([
            'studentId' => $studentId,
        ]);

        $answeredQuestions = $response->original->getData()['report']->students;

        $this->assertEquals($num, $answeredQuestions[$studentId]['count']);
    }

    /**
     * @Given student :arg1 has completed the following questions
     */
    public function studentHasCompletedTheFollowingQuestions($index, TableNode $table)
    {
        $studentId      = $this->studentIds[$index - 1];
        $client         = new ApiClient;
        $problemFactory = new DatabaseProblemFactory;

        foreach ($table->getHash() as $row) {
            $createProblem = new CreateProblem($this->repo, $problemFactory);
            $problemId     = $createProblem->execute([
                'unit_id'   => $row['Unit ID'],
                'title'     => $row['Question'],
                'config_id' => 'syntax.syntax1'
            ]);

            $this->problemIds[] = $problemId;

            $client->answerProblem([
                'student_id' => $studentId,
                'answer'     => $row['Answer'],
                'problem_id' => $problemId
            ]);
        }
    }

    /**
     * @Then the report for student :arg1 shows:
     */
    public function theReportForStudentShows($index, TableNode $table)
    {
        $this->client->actingAs($this->admin);

        $studentId = $this->studentIds[$index - 1];

        $response = $this->client->getReport([
            'studentId' => $studentId,
        ]);

        $answeredQuestions = $response
            ->original
            ->getData()['report']
            ->answeredQuestions;

        $found = false;

        foreach ($table->getHash() as $row) {
            $answer = $this->answersContain($answeredQuestions, $row);
            if ($answer) {
                $found = true;
            }
        }

        if (!$found) {
            throw new \Exception(
                "Answer not found for question {$row['Question']}"
            );
        }
    }

    public function answersContain($answeredQuestions, $row)
    {
        foreach ($answeredQuestions as $answer) {
            if ($answer->answer == $row['Answer'] && $answer->question == $row['Question']) {
                return true;
            }
        }

        return false;
    }

    /**
     * @When I change question :arg1 to :arg2
     */
    public function iChangeQuestionTo($title, $newTitle)
    {
        foreach ($this->problemIds as $id) {
            $existingProblem = $this->repo->problemWithId($id, $title);
            if ($existingProblem->title == $title) {
                $existingProblem->title = $newTitle;
                $this->repo->updateProblem($existingProblem);
            }
        }
    }

    /**
    * @Given there are the following units
    */
    public function thereAreTheFollowingUnits(TableNode $table)
    {
        $this->admin = factory(User::class)->create();

        $unitFactory = new DatabaseUnitFactory;

        foreach ($table->getHash() as $row) {
            $createUnit = new CreateUnit($this->repo, $unitFactory);
            $unitId     = $createUnit->execute([
               'name' => $row['Name'],
               'slug' => str_slug($row['Name'])
           ]);

            $this->unitIds[] = $unitId;

            $this->client->createUnit([
               'user' => $this->admin,
               'name' => $row['Name']
           ]);
        }
    }

    /**
      * @Given there is a unit :arg1 with questions
      */
    public function thereIsAUnitWithQuestions($unitName)
    {
        $this->client = new ApiClient;
        $this->admin  = factory(User::class)->create();

        $unitFactory = new DatabaseUnitFactory;

        $createUnit = new CreateUnit($this->repo, $unitFactory);
        $unitId     = $createUnit->execute([
            'name' => $unitName,
            'slug' => str_slug($unitName)
        ]);

        $this->unitIds[] = $unitId;

        $this->client->createUnit([
            'user' => $this->admin,
            'name' => $unitName
        ]);

        $problemFactory = new DatabaseProblemFactory;

        for ($i=1; $i < 6; $i++) {
            $createProblem = new CreateProblem($this->repo);
            $problemId     = $createProblem->execute([
                'unit_id'   => $unitId,
                'config_id' => $unitName.'.'.$unitName.$i,
                'title'     => 'questionTitle'.$i
            ]);

            $this->problemIds[] = $problemId;
        }
    }

    /**
     * @When I log in as a student
     */
    public function iLogInAsAStudent()
    {
        $id = $this->thereIsAStudent('studentName');

        $this->student = User::find($id);

        $this->client->actingAs($this->student);
    }

    /**
     * @When I answer all the questions for unit :arg1
     */
    public function iAnswerAllTheQuestionsForUnit($unitName)
    {
        $studentId = $this->studentIds[0];

        $getUnitQuestions = new GetUnitQuestions($this->repo);
        $questions        = $getUnitQuestions->execute([
            'unit_id' => $this->unitIds[0]
        ]);

        foreach ($questions as $question) {
            $this->client->answerProblem([
                'student_id' => $studentId,
                'answer'     => 'studentAnswer',
                'problem_id' => $question->id
            ]);
        }
    }

    /**
     * @Then the admin can see my progress
     */
    public function theAdminCanSeeMyProgress()
    {
        $this->client->actingAs($this->admin);

        $studentId = $this->studentIds[0];

        $response = $this->client->getReport([
            'studentId' => $studentId,
        ]);

        $answeredQuestions = $response
            ->original
            ->getData()['report']
            ->answeredQuestions;

        $found = false;

        foreach ($answeredQuestions as $answer) {
            if ($answer->answer == 'studentAnswer') {
                $found = true;
            }
        }

        if (!$found) {
            throw new \Exception(
                "Answer not found for question {$row['Question']}"
            );
        }
    }

    /**
     * @Given there is a unit :arg1 with :arg2 questions
     */
    public function thereIsAUnitWithQuestions2($unitName, $numberOfQuestions)
    {
        $this->client = new ApiClient;
        $this->admin  = factory(User::class)->create([
            'is_admin' => true
        ]);

        $config = $this->repo->unitConfigWithName($unitName);

        $createUnit = new CreateUnit($this->repo, $config);
        $unitId     = $createUnit->execute([
            'name' => $unitName,
            'slug' => str_slug($unitName)
        ]);

        $this->unitIds[] = $unitId;

        $problemFactory = new DatabaseProblemFactory;

        for ($i=1; $i <= $numberOfQuestions; $i++) {
            $createProblem = new CreateProblem($this->repo, $problemFactory);
            $problemId     = $createProblem->execute([
                'unit_id'   => $unitId,
                'config_id' => str_slug($unitName).'.'.str_slug($unitName).$i,
                'title'     => 'questionTitle'.$i
            ]);

            $this->problemIds[] = $problemId;
        }
    }

    /**
     * @When I start a lesson
     */
    public function iStartALesson()
    {
        $studentId  = $this->studentIds[0];
        $this->next = (object) [
            'unit'   => 'syntax',
            'lesson' => 1
        ];
    }

    /**
     * @Then I can see question :arg2 from unit :arg1
     */
    public function iCanSeeQuestionFromUnit($questionIndex, $unitName)
    {
        $response = $this->client->showQuestion([
            'unitId'     => $this->next->unit,
            'questionId' => $this->next->lesson,
            'studentId'  => $this->student->id,
        ]);

        $viewData = $response->original->getData();

        $this->assertEquals($unitName, strtolower($viewData['unit']));
        $this->assertEquals(
            $questionIndex,
            $viewData['lesson'],
            "Expected to see question $questionIndex"
        );
    }

    /**
     * @Then I answer question :arg2 for unit :arg1
     */
    public function iAnswerQuestionForUnit($questionIndex, $unitName)
    {
        $studentId = $this->studentIds[0];
        $problemId = $this->problemIds[$questionIndex - 1];

        $correctAnswers = [
            'syntax.syntax1' => 'echo "Manual Override Function Enabled.";',
            'syntax.syntax2' => '$i = "D F jS, Y"; echo date($i);',
            'loops.loops1'   => 'echo "Manual Override Function Enabled.";',
            'loops.loops2'   => '$i = "D F jS, Y"; echo date($i);',
        ];

        $pureAnswer = $correctAnswers[$unitName.'.'.$unitName.$questionIndex];

        $answer = "<?php\n\n" . $pureAnswer;

        $response = $this->client->answerQuestion([
            'unit_id'     => $unitName,
            'student_id'  => $studentId,
            'question_id' => $problemId,
            'code'        => $answer
        ]);

        $responseData = $response->getData();

        $this->assertTrue(
            $responseData->success,
            "The answer '$pureAnswer' for question $unitName.$unitName{$problemId} was incorrect."
        );

        $this->next = $responseData->next ?? false;
    }

    /**
     * @Then I am done
     */
    public function iAmDone()
    {
        $this->assertFalse($this->next);
    }
}
