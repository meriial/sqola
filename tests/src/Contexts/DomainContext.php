<?php

namespace App\Tests\Contexts;

use App\Tests\Helpers\PHPUnit;
use App\Tests\Repositories\InMemoryRepo;
use Behat\Behat\Context\Context;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;

use Faker\Factory;
use Sqola\Entities\Admin;
use Sqola\Entities\Lesson;
use Sqola\Entities\Problem;
use Sqola\Entities\Student;
use Sqola\Entities\Unit;
use Sqola\Services\Course;
use Sqola\Services\ProgressReport;
use Sqola\Values\UnitConfig;
use Sqola\Values\CourseConfig;

/**
 * Defines application features from the specific context.
 */
class DomainContext extends MinkContext implements Context
{
    use PHPUnit;

    private $students = [];
    private $problems = [];
    private $units    = [];

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->faker = Factory::create();
        $this->repo  = new InMemoryRepo();
    }

    /**
     * @Given there is a student :arg1
     */
    public function thereIsAStudent($studentName)
    {
        $id               = rand(1, 10000);
        $this->students[] = new Student($id, $studentName, $this->faker->email);
    }

    /**
     * @Given student :arg1 has completed :arg2 questions
     */
    public function studentHasCompletedQuestions($index, $num)
    {
        for ($i=0; $i < $num; $i++) {
            $question = new Problem('what is the meaning of life?');
            $answer   = $this->studentAtIndex($index)->answer($question, '42');
            $this->repo->saveAnswer($answer);
        }
    }

    private function studentAtIndex($index)
    {
        return $this->students[$index - 1];
    }

    private function problemAtIndex($index)
    {
        return $this->problems[$index - 1];
    }

    /**
     * @When an admin views the progress area
     */
    public function anAdminViewsTheProgressArea()
    {
    }

    /**
     * @Then student :arg1 shows :arg2 questions completed
     */
    public function studentShowsQuestionsCompleted($index, $num)
    {
        $student = $this->studentAtIndex($index);

        $progressReport = new ProgressReport($this->repo, $student);

        $completedCount = $progressReport->countCompleted();

        $this->assertEquals($num, $completedCount);
    }

    /**
     * @Given student :arg1 has completed the following questions
     */
    public function studentHasCompletedTheFollowingQuestions($index, TableNode $table)
    {
        $student = $this->studentAtIndex($index);

        foreach ($table->getHash() as $row) {
            $id               = rand(1, 10000);
            $this->problems[] = new Problem(
                $id,
                $row['Unit ID'],
                'syntax.syntax1',
                $row['Question']
            );

            $problem = $this->problemAtIndex($index);
            $answer  = $this
                ->studentAtIndex($index)
                ->answer($problem, $row['Answer']);

            $this->repo->saveAnswer($answer);
        }
    }

    /**
     * @Then the report for student :arg1 shows:
     */
    public function theReportForStudentShows($index, TableNode $table)
    {
        $student = $this->studentAtIndex($index);

        $progressReport = new ProgressReport($this->repo, $student);

        $answers = $progressReport->answeredQuestions();

        $found = false;

        foreach ($table->getHash() as $row) {
            $answer = $this->answersContain($answers, $row);
            if ($answer) {
                $found = true;
            }
        }

        if (!$found) {
            throw new \Exception(
                "Answer not found for question {$row['Question']}"
            );
        }
    }

    public function answersContain($answers, $row)
    {
        foreach ($answers as $answer) {
            if ($answer->problem->title == $row['Question']) {
                if ($answer->problem->unit_id == $row['Unit ID']) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @When I change question :arg1 to :arg2
     */
    public function iChangeQuestionTo($title, $newTitle)
    {
        foreach ($this->problems as $problem) {
            if ($problem->title = $title) {
                $problem->title = $newTitle;
            }
        }
    }

    /**
     * @Given there are the following units
     */
    public function thereAreTheFollowingUnits(TableNode $table)
    {
        foreach ($table->getHash() as $row) {
            $this->units[] = new Unit($row['Id'], $row['Name']);
        }
    }

    /**
     * @Given there is a unit :arg1 with questions
     */
    public function thereIsAUnitWithQuestions($unitName)
    {
        $unitId        = rand(1, 10000);
        $this->units[] = new Unit($unitId, $unitName);

        for ($i=1; $i < 6; $i++) {
            $this->problems[] = new Problem($i, $unitId, 'configId', 'questionTitle');
        }
    }

    /**
     * @When I log in as a student
     */
    public function iLogInAsAStudent()
    {
        $this->thereIsAStudent('student');
    }

    /**
     * @When I answer all the questions for unit :arg1
     */
    public function iAnswerAllTheQuestionsForUnit($unitName)
    {
        foreach ($this->problems as $problem) {
            $answer = $this
                ->studentAtIndex(1)
                ->answer($problem, 'studentsAnswer');

            $this->repo->saveAnswer($answer);
        }
    }

    /**
     * @Then the admin can see my progress
     */
    public function theAdminCanSeeMyProgress()
    {
        $student = $this->studentAtIndex(1);

        $progressReport = new ProgressReport($this->repo, $student);

        $answers = $progressReport->answeredQuestions();

        $found = false;

        foreach ($answers as $answer) {
            if ($answer->answer == "studentsAnswer") {
                $found = true;
            }
        }

        if (!$found) {
            throw new \Exception(
                "Expected answer of 'studentsAnswer' not found."
            );
        }
    }

    /**
     * @When I am an admin
     */
    public function iAmAnAdmin()
    {
        $id          = rand(1, 10000);
        $name        = $this->faker->name;
        $this->admin = new Admin($id, $name, $this->faker->email);
    }

    /**
     * @Then I can view student progress
     */
    public function iCanViewStudentProgress()
    {
        throw new PendingException();
    }

    /**
     * @Then I can manage units and questions
     */
    public function iCanManageUnitsAndQuestions()
    {
        throw new PendingException();
    }

    /**
     * @When I am not an admin
     */
    public function iAmNotAnAdmin()
    {
        throw new PendingException();
    }

    /**
     * @Then I cannnot view student progress
     */
    public function iCannnotViewStudentProgress()
    {
        throw new PendingException();
    }

    /**
     * @Then I cannnot manage units and questions
     */
    public function iCannnotManageUnitsAndQuestions()
    {
        throw new PendingException();
    }

    /**
     * @Given there is a unit :arg1 with :arg2 questions
     */
    public function thereIsAUnitWithQuestions2($unitName, $numberOfQuestions)
    {
        $config = new CourseConfig([]);

        $this->course = $this->course ?? new Course($this->repo, $config);

        $config = [];
        for ($i=1; $i <= $numberOfQuestions; $i++) {
            $configId          = $unitName.$i;
            $config[$configId] = [];
        }

        $unit = $this->repo->createUnit($unitName, new UnitConfig($config));

        $this->course->addUnit($unit);

        for ($i=1; $i <= $numberOfQuestions; $i++) {
            $this->repo->createProblem([
                'unitSlug' => $unitName,
                'configId' => $unitName.'.'.$unitName.$i,
                'title'    => 'questionTitle'
            ]);
        }
    }

    /**
     * @When I start a lesson
     */
    public function iStartALesson()
    {
        $this->student = $this->students[0];

        $this->lesson = $this->course->start();
        $question     = $this->lesson->start();

        $this->next = (object) [
            'unit'   => $question->unit->name,
            'lesson' => $question->index()
        ];
    }

    /**
     * @Then I can see question :arg2 from unit :arg1
     */
    public function iCanSeeQuestionFromUnit($questionIndex, $unitName)
    {
        $question = $this->lesson->currentQuestion();

        $this->assertEquals($unitName, $question->unit->slug);
        $this->assertEquals($questionIndex, $question->index());
    }

    /**
     * @Then I answer question :arg2 for unit :arg1
     */
    public function iAnswerQuestionForUnit($questionIndex, $unitName)
    {
        $student  = $this->students[0];
        $question = $this->lesson->currentQuestion();

        $answer = $this->lesson->studentAnswers($student, $question, 'studentsAnswer');

        $this->repo->saveAnswer($answer);

        $next = $this->lesson->nextQuestion();

        if ($next->isTheEnd()) {
            $this->lesson = $this->course->nextLesson();

            if ($this->lesson->isTheEnd()) {
                return;
            }

            $this->lesson->start();
            return;
        }

        $this->next = (object) [
            'unit'   => $next->unit->name,
            'lesson' => $next->index()
        ];
    }

    /**
     * @Then I am done
     */
    public function iAmDone()
    {
        $this->assertTrue($this->lesson->isTheEnd(), "You should be done, but you aren't.");
    }
}
