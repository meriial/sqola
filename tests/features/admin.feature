Feature: Admin can see students' progress

@api @app @domain @browser
Scenario: Admin views progress
    Given there is a student "a"
    And there is a student "a"
    And student 1 has completed the following questions
    | Unit ID | Question                    | Answer    |
    | 1       | what is the meaning of life | 42        |
    | 1       | what is 2 + 2               | 4         |
    | 1       | what is peanut butter       | delicious |
    And student 2 has completed the following questions
    | Unit ID | Question                    | Answer    |
    | 1       | what is the meaning of life | 42        |
    | 1       | what is peanut butter       | delicious |
    When an admin views the progress area
    Then student 1 shows 3 questions completed
    Then student 2 shows 2 questions completed
    And the report for student 1 shows:
    | Unit ID | Question                    | Answer    |
    | 1       | what is the meaning of life | 42        |
    | 1       | what is 2 + 2               | 4         |
    | 1       | what is peanut butter       | delicious |
    When I change question "what is peanut butter" to "what is jam"
    Then the report for student 1 shows:
    | Unit ID | Question                    | Answer    |
    | 1       | what is the meaning of life | 42        |
    | 1       | what is 2 + 2               | 4         |
    | 1       | what is jam                 | delicious |

@browser
Scenario: Only admins can view admin section
    When I am an admin
    Then I can view student progress
    And I can manage units and questions
    When I am not an admin
    Then I cannnot view student progress
    And I cannnot manage units and questions
