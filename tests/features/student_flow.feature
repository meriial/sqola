Feature: Students can answer question

@domain @app @api @browser
Scenario: Student answers quiz
    Given there is a unit "syntax" with questions
    When I log in as a student
    And I answer all the questions for unit "syntax"
    Then the admin can see my progress
