<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Factories\DatabaseUnitFactory;
use App\Factories\DatabaseProblemFactory;
use App\Repositories\DbRepo;
use Sqola\Commands\CreateUnit;
use Sqola\Commands\CreateCourse;
use Sqola\Commands\CreateProblem;
use Sqola\Services\Course;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->makeUser();
        $this->makeContent();
    }

    public function makeUser()
    {
        factory(User::class)->create([
            'email' => 'cameron@twosmiles.ca',
            'password' => bcrypt('12341234'),
            'is_admin' => true
        ]);
    }

    public function makeContent()
    {
        $repo  = new DbRepo();

        $x = glob(resource_path('units').'/*');

        $units = [];
        foreach ($x as $y) {
            $units[] = basename($y, '.php');
        }

        $createCourse = new CreateCourse($repo);
        $createCourse->execute([
            'units' => $units
        ]);
    }
}
