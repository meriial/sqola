<?php

namespace Sqola\Entities;

class Admin
{
    public function __construct(int $id, $name, $email)
    {
        $this->id    = $id;
        $this->name  = $name;
        $this->email = $email;
    }
}
