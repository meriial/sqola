<?php

namespace Sqola\Entities;

use Sqola\Entities\Student;

class Answer
{
    public function __construct(Problem $problem, Student $student, string $answer)
    {
        $this->problem = $problem;
        $this->student = $student;
        $this->answer  = $answer;
    }
}
