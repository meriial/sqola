<?php

namespace Sqola\Entities;

use Sqola\Values\UnitConfig;
use Sqola\Entities\Problem;

class Unit
{
    private $questions = [];

    public function __construct($id, UnitConfig $config, $name, $slug)
    {
        $this->id     = $id;
        $this->config = $config;
        $this->name   = $name;
        $this->slug   = $slug;
    }

    public function equals(Unit $unit)
    {
        return $this->id == $unit->id;
    }

    public function addQuestion(Problem $question)
    {
        $this->questions[] = $question;
    }

    public function firstQuestion(): Problem
    {
        return $this->questionAtIndex(0);
    }

    public function questionAtIndex(int $index)
    {
        if (empty($this->questions)) {
            throw new \Exception("The unit '$this->name' has no questions.");
        }

        if (!isset($this->questions[$index])) {
            var_dump( $this );
            throw new \Exception("The unit '$this->name' has no question at index $index.");
        }

        return $this->questions[$index];
    }

    public function questionAfter(Problem $question): Problem
    {
        $i = false;

        foreach ($this->questions as $i => $q) {
            if ($q->equals($question)) {
                break;
            }
        }

        if ($i === false || empty($this->questions[$i+1])) {
            return new NullQuestion();
        }

        return $this->questions[$i+1];
    }

    public function configforQuestion(Problem $question)
    {
        return $this->config->config[$question->configIndex()];
    }
}
