<?php

namespace Sqola\Entities;

class NullQuestion extends Problem
{
    protected $theEnd = true;

    public function __construct()
    {
    }

    public function configIndex()
    {
        throw new \Exception("Trying to access the configIndex of a null question.");
    }
}
