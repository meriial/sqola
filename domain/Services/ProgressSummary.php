<?php

namespace Sqola\Services;

class ProgressSummary
{
    public function __construct($repo)
    {
        $this->repo = $repo;
    }

    public function students()
    {
        $output = [];

        $students = $this->repo->allStudents();
        foreach ($students as $student) {
            $report = new ProgressReport($this->repo, $student);

            $output[$student->id] = [
                'student'   => $student->name,
                'count' => $report->countCompleted()
            ];
        }

        return $output;
    }
}
