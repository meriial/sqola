<?php

namespace Sqola\Contracts;

use Sqola\Entities\Student;
use Sqola\Entities\Problem;
use Sqola\Entities\Unit;
use Sqola\Values\UnitConfig;

interface Repository
{
    public function saveStudent(Student $student);

    public function createProblem($payload): Problem;
    public function createUnit($unitName, UnitConfig $config): Unit;

    public function studentWithId($id);

    // public function problemWithTitle($title);

    public function problemWithId($id);

    public function questionsWithUnitId($id);

    public function unitWithSlug($slug): Unit;

    public function questionWithConfigId($configId): Problem;
}
