<?php

namespace Sqola\Commands;

class GetUnitBySlug extends Command
{
    public function execute($payload)
    {
        $unit = $this->repo->unitWithSlug($payload['slug']);

        return $unit;
    }
}
