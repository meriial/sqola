<?php

namespace Sqola\Commands;

use Sqola\Entities\Lesson;

class AnswerProblem extends Command
{
    public function execute($payload)
    {
        $studentId     = $payload['student_id'];
        $answer        = $payload['answer'];
        $unitSlug      = $payload['unitSlug'];
        $questionIndex = $payload['questionIndex'];

        $student  = $this->repo->studentWithId($studentId);
        $unit     = $this->repo->unitWithSlug($unitSlug);
        $question = $unit->questionAtIndex($questionIndex - 1);

        \Log::error('answering problem for ', [$student]);

        if (!$student) {
            throw new \Exception("Student with id '$id' not found.");
        }

        $answer = $student->answer($question, $answer);
        $this->repo->saveAnswer($answer);

        $lesson = new Lesson($this->repo, $question->unit);
        $lesson->setQuestion($question);

        $output   = $lesson->run($answer);
        $expected = $lesson->expected();

        // \Log::error('@@@@@@@@', compact('output', 'expected', 'answer'));
        $success = ($output == $expected);

        if ($success) {
            $next = $lesson->nextQuestion();
        } else {
            $next = $lesson->currentQuestion();
        }

        if ($next->isTheEnd()) {
            $course = $this->repo->getCourse();
            $course->setLesson($lesson);
            $lesson = $course->nextLesson();
            if ($lesson->isTheEnd()) {
                return [
                    'success' => $success,
                    'output'  => $output,
                ];
            } else {
                $next = $lesson->start();
            }
        }

        \Log::error('status of question: '.$question->configIndex(), [$success]);
        \Log::error('---------------------- getting next question:'.$next->configIndex());
        \Log::error('---------------------- getting next unit:'.$next->unit->name);

        return [
            'success' => $success,
            'output'  => $output,
            'unit'    => $next->unit->name,
            'lesson'  => $next->index(),
            'next'    => $next->configIndex()
        ];
    }
}
