<?php

namespace Sqola\Commands;

class GetStudentById extends Command
{
    public function execute($payload)
    {
        $student = $this->repo->studentWithId($payload['id']);

        return $student;
    }
}
