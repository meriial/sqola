<?php

namespace Sqola\Commands;

use Sqola\Entities\Lesson;

class GetLesson extends Command
{
    public function execute($payload)
    {
        extract($payload);

        $student = $this->repo->studentWithId($studentId);
        $unit    = $this->repo->unitWithName($unitName);

        $lesson = new Lesson($this->repo, $unit);
        $lesson->goToQuestion($lessonIndex);

        $input = $lesson->input();

        if (!is_string($input)) {
            $input = $this->renderInput($input);
        }

        return [
            'expected' => $lesson->expected(),
            'input'    => $input,
            'preamble' => $lesson->preamble(),
            'scenario' => $lesson->scenario(),
            'unit'     => $unitName,
            'lesson'   => $lessonIndex
        ];
    }

    public function renderInput($input)
    {
        $output = '<?php'."\n\n\n";

        foreach ($input as $key => $value) {
            if (is_array($value)) {
                if ($this->isSequential($value)) {
                    $value = '["'.implode('", "', $value).'"]';
                } else {
                    $tmp = "[\n";
                    foreach ($value as $aKey => $aValue) {
                        $tmp .= "\t'$aKey' => '$aValue',\n";
                    }
                    $tmp .= "]";
                    $value = $tmp;
                }
            } else {
                $value = "'$value'";
            }
            $output .= '$'.$key.' = '.$value.";\n";
        }

        return $output;
    }

    public function isSequential($array)
    {
        return count(array_filter(array_keys($array), 'is_string')) == 0;
    }
}
