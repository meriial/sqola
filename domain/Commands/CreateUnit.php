<?php

namespace Sqola\Commands;

use Sqola\Entities\Unit;
use Sqola\Contracts\Repository;
use Sqola\Contracts\UnitFactory;
use Sqola\Values\UnitConfig;

class CreateUnit extends Command
{
    public function __construct(Repository $repo, UnitConfig $config)
    {
        $this->repo = $repo;
        $this->config = $config;
    }

    public function execute($payload)
    {
        extract($payload);

        $unit = $this->repo->createUnit($name, $this->config);

        return $unit->id;
    }
}
