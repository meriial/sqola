<?php

namespace Sqola\Commands;

use Sqola\Entities\Problem;
use Sqola\Contracts\Repository;
use Sqola\Contracts\ProblemFactory;

class CreateProblem extends Command
{
    public function __construct(Repository $repo)
    {
        $this->repo = $repo;
    }

    public function execute($payload)
    {
        $problem = $this->repo->createProblem($payload);

        return $problem->id;
    }
}
