<?php

namespace Sqola\Commands;

class GetUnits extends Command
{
    public function execute()
    {
        $units = $this->repo->getUnits();

        return $units;
    }
}
