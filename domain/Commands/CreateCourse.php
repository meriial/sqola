<?php

namespace Sqola\Commands;
use Sqola\Values\CourseConfig;

class CreateCourse extends Command
{
    public function execute($payload)
    {
        extract($payload);

        $courseConfig = new CourseConfig([]);

        foreach ($units as $unitSlug) {
            try {
                $unit = $this->repo->unitWithSlug($unitSlug);
            } catch (\Exception $e) {
                $config = $this->repo->unitConfigWithName($unitSlug);
                $unit = $this->repo->createUnit($unitSlug, $config);
            }

            foreach ($unit->config->config as $configId => $questionConfig) {
                try {
                    $existing = $this->repo->questionWithConfigId($unitSlug.'.'.$configId);
                } catch (\Exception $e) {
                    $this->repo->createProblem([
                        'unit_id' => $unit->id,
                        'config_id' => $unitSlug.'.'.$configId,
                        'title' => 'DUMMY'
                    ]);
                }
            }
        }
    }
}
