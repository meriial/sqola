<?php

namespace Sqola\Commands;

abstract class Command
{
    public function __construct($repo)
    {
        $this->repo = $repo;
    }
}
