<?php

namespace Sqola\Commands;

use Sqola\Entities\Lesson;

class CreateLesson extends Command
{
    public function execute($payload)
    {
        $lesson = new Lesson($payload['student'], $payload['unit'], $payload['index']);

        return $lesson;
    }
}
