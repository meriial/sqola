<?php

namespace Sqola\Commands;

class GetUnitQuestions extends Command
{
    public function execute($payload)
    {
        $questions = $this->repo->problemsWithUnitId($payload['unit_id']);

        return $questions;
    }
}
